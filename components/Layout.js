import h from 'react-hyperscript'
import styled from 'styled-components'

import Link from 'next/link'
import Head from 'next/head'

export const Layout = (props) => {
  return h(Main, [
    h(Head, [
      h('title', props.title)
    ]),
    h(Link, {href: '/', prefetch: true}, h('a', 'home')),
    props.children
  ])
}

const Main = styled('div')`
margin: auto;
max-width: 800px;
padding: 10px;
line-height:1.6;
font-size: 1.2em;
`
