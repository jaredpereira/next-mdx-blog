import h from 'react-hyperscript'
import Link from 'next/link'

function importAll(r) {
  return r.keys().map(r);
}

export const Posts = importAll(require.context('../pages/blog'), false, /(?!index)\.mdx$/)

export const BlogList = () => {
  let postComponents = Posts.filter(post => post.meta)
      .sort((a, b) => {
        let aDate = new Date(a.meta.date)
        let bDate = new Date(b.meta.date)
        if (aDate > bDate) return -1
        return 1
      })
      .map((post) => {
        return h('li', [
          h(Link, {href: 'blog/' + post.meta.filename, prefetch: true},
            h('a', post.meta.title)),
          h('span', '  ' + post.meta.date)
        ])
      })
  return h('ul', postComponents)
}
