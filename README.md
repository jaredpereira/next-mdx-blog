![Next-MDX-Blog](https://og-image.now.sh/A%20**minimal**%20blog%20boilerplate%20using%20NextJS%20and%20MDX.png?theme=light&md=1&fontSize=50px&images=https%3A%2F%2Fassets.zeit.co%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg&images=https%3A%2F%2Fmdx-logo.now.sh)

This is a pretty small starter template. Fork it, change it, make it yours! 

It's goals are: 

- To be as small as possible
- To be easy to understand, in implementation and motivation
- A clean developer experience

Check it out, and read a bit more about the considerations at [next-mdx-blog.now.sh](https://next-mdx-blog.now.sh/)

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Getting Started](#getting-started)
    - [Clone the repository](#clone-the-repository)
    - [Remove the cruft](#remove-the-cruft)
    - [Install Dependencies](#install-dependencies)
    - [Start developing!](#start-developing)
- [Features](#features)
    - [Hot-Reloading Everything](#hot-reloading-everything)
    - [RSS/Atom](#rssatom)
- [Extras](#extras)
    - [Typescript Support](#typescript-support)

<!-- markdown-toc end -->

# Getting Started

You're going to need to know how to use the terminal, node/npm, and git!

## Clone the repository

```
git clone https://gitlab.com/jaredpereira/next-mdx-blog.git

cd next-mdx-blog
```

## Remove the cruft

```
rm -rf .git && git init && npm init
```

This'll remove the git history of this repository and let you start fresh!

## Install Dependencies

```
npm i
```

## Start developing!

```
npm run dev
```

This'll start the next development server and serve up your site on [localhost:3000](http://localhost:3000)

Now you can modify any of the files and get to building your site!

# Features

## Hot-Reloading Everything

Some blog templates require you to run a script to generate an index of all your
blogs. Here we use webpack's `require` to avoid that, so you can just add pages
to your blog and see them immediately reflected everywhere.

## RSS/Atom

Using the same mechanism we automatically generate a rss and atom feed when you
publish your site. We take advantage of `exportPathMap` in next's config file to
run a script that creates them.

# Extras

## Typescript Support

We use a [nifty next
plugin](https://github.com/zeit/next-plugins/tree/master/packages/next-typescript)
to enable typescript support and include a `tsconfig.json` file, so you can just
drop in a Typescript file and it'll work!
