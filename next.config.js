const withMDX = require('@zeit/next-mdx')({
  extension: /\.mdx?$/,
})
const withTS = require('@zeit/next-typescript')

module.exports = withTS(withMDX({
  pageExtensions:['ts', 'tsx', 'js', 'jsx', 'mdx'],
  webpack: (config, { dev, isServer }) => {
    if (isServer && !dev) {
      const originalEntry = config.entry;
      config.entry = async () => {
        const entries = { ...(await originalEntry()) };

        entries['./scripts/generateRSS.js'] = './scripts/generateRSS.js';
        return entries;
      };
    }
    return config
  },
  exportPathMap(defaultPathMap, {dev, outDir}) {
    if(!dev){
      const generateRSS = require('./.next/server/scripts/generateRSS.js').default
      generateRSS(outDir)
    }
    return defaultPathMap
  }
}))
