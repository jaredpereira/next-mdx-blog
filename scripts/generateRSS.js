import * as fs from 'fs'
import {Feed} from 'feed'
import * as path from 'path'
import {Posts} from '../components/BlogList'

// Modify this! Fill in you're deets
export default (dir) => {
  let site = 'https://next-mdx-blog.now.sh/'
  let feed = new Feed({
    title: 'awarm.space',
    link: site,
    id: site,
    author: {
      name: "Jared Pereira",
      email: "jared@awarm.space"
    },
    feedLinks: {
      atom: site + 'atom.xml',
      rss: site + 'rss.xml'
    },
    copyright: 'CC0'
  })

  Posts.map(post => {
    if(!post.meta) return
    feed.addItem({
      title: post.meta.title,
      link: site + 'blog/' + post.meta.filename,
      date: new Date(post.meta.date),
      description: post.meta.blurb
    })
  })

  fs.writeFileSync(path.join(dir, 'atom.xml'), feed.atom1())
  fs.writeFileSync(path.join(dir, 'rss.xml'), feed.rss2())
}
